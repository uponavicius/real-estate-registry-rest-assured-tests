# real-estate-registry-rest-assured-tests

Real estate registry rest-assured tests

With this project can test `real-estate-registry` project https://gitlab.com/uponavicius/real-estate-registry

### To start project:
* Clone repository https://gitlab.com/uponavicius/real-estate-registry-rest-assured-tests.git
* Open with your favorite IDEA (example IntelliJ IDEA)
* ATTENTION! First must run `real-estate-registry` project. If running, then restart - need reset H2 memory database.
* In `real-estate-registry-rest-assured-tests` project go to `src\test\java\com\upondev\realestateregistryrestassuredtests` and run this class `RealEstateRegistryRestAssuredTestsApplicationTests`

