package com.upondev.realestateregistryrestassuredtests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestOwnerAndBuildingWebServiceEndpoint {
    String applicationJson = "application/json";
    Map<String, Object> ownerDetails;
    static String ownerId;
    Map<String, Object> building;
    static String buildingId;

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
        createOwnerDetails();
        createBuild();
    }

    /**
     * Test create owner with buildings
     */
    @Test
    void a() {
        Response response = given()
                .contentType(applicationJson)
                .accept(applicationJson)
                .body(ownerDetails)
                .when()
                .post("/owner")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);
        ownerId = response.jsonPath().getString("id");
        System.out.println(ownerId);
        assertNotNull(response.jsonPath().getString("id"));
        assertEquals(response.jsonPath().getString("firstName"), "Vladas");
        assertEquals(response.jsonPath().getString("lastName"), "Uponavicius");
        assertEquals(response.jsonPath().getLong("phone"), 37060000000L);
    }

    /**
     * Test get all owners
     */
    @Test
    void b() {
        Response response = given()
                .contentType(applicationJson)
                .accept(applicationJson)
                .when()
                .get("/owner")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);

        List<Object> owners = response.jsonPath().getList("");
        assertEquals(1, owners.size());
    }

    /**
     * Get owner by id
     */
    @Test
    void c() {
        Response response = given()
                .contentType(applicationJson)
                .pathParam("ownerId", ownerId)
                .accept(applicationJson)
                .when()
                .get("/owner/" + "{ownerId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        System.out.println(ownerId);
        assertNotNull(response);
        assertEquals(response.jsonPath().getString("firstName"), "Vladas");
        assertEquals(response.jsonPath().getString("lastName"), "Uponavicius");
        assertEquals(response.jsonPath().getLong("phone"), 37060000000L);
    }

    /**
     * Test update owner details
     */
    @Test
    void d() {
        Map<String, Object> ownerDetailsUpdate = new HashMap<>();
        ownerDetailsUpdate.put("firstName", "John");
        ownerDetailsUpdate.put("lastName", "Smith");
        ownerDetailsUpdate.put("phone", "37060000001");


        Response response = given()
                .contentType(applicationJson)
                .body(ownerDetailsUpdate)
                .pathParam("ownerId", ownerId)
                .accept(applicationJson)
                .when()
                .put("/owner/" + "{ownerId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);
        assertEquals(response.jsonPath().getString("firstName"), "John");
        assertEquals(response.jsonPath().getString("lastName"), "Smith");
        assertEquals(response.jsonPath().getLong("phone"), 37060000001L);
    }

    /**
     * Test create building
     */
    @Test
    void e() {
        Response response = given()
                .contentType(applicationJson)
                .accept(applicationJson)
                .pathParam("ownerId", ownerId)
                .body(building)
                .when()
                .post("/owner/" + "{ownerId}" + "/building")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);
        buildingId = response.jsonPath().getString("id");
        assertNotNull(response.jsonPath().getString("id"));
        assertEquals("House", response.jsonPath().getString("propertyType"));
        assertEquals("Vilnius", response.jsonPath().getString("city"));
        assertEquals("Vingio", response.jsonPath().getString("street"));
        assertEquals("35", response.jsonPath().getString("number"));
        assertEquals(180.0, Double.parseDouble(response.jsonPath().getString("size").toString()));
        assertEquals(180000.0, Double.parseDouble(response.jsonPath().getString("marketValue").toString()));
    }

    /**
     * Test get all owner buildings
     */
    @Test
    void f() {
        Response response = given()
                .contentType(applicationJson)
                .accept(applicationJson)
                .pathParam("ownerId", ownerId)
                .body(building)
                .when()
                .get("/owner/" + "{ownerId}" + "/building")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();
        assertNotNull(response);

        List<Map<String, Object>> buildings = response.jsonPath().getList("buildings");
        assertEquals(1, buildings.size());
    }

    /**
     * Get building
     */
    @Test
    void g() {
        Response response = given()
                .contentType(applicationJson)
                .accept(applicationJson)
                .pathParam("ownerId", ownerId)
                .pathParam("buildingId", buildingId)
                .body(building)
                .when()
                .get("/owner/" + "{ownerId}" + "/building/" + "{buildingId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);
        assertNotNull(response.jsonPath().getString("id"));
        assertEquals("House", response.jsonPath().getString("propertyType"));
        assertEquals("Vilnius", response.jsonPath().getString("city"));
        assertEquals("Vingio", response.jsonPath().getString("street"));
        assertEquals("35", response.jsonPath().getString("number"));
        assertEquals(180.0, Double.parseDouble(response.jsonPath().getString("size").toString()));
        assertEquals(180000.0, Double.parseDouble(response.jsonPath().getString("marketValue").toString()));
    }

    /**
     * Test update building
     */
    @Test
    void h() {
        Map<String, Object> updateBuilding = new HashMap<>();
        updateBuilding.put("propertyType", "Apartment");
        updateBuilding.put("city", "Klaipeda");
        updateBuilding.put("street", "Kranto");
        updateBuilding.put("number", "35-2");
        updateBuilding.put("size", "75");
        updateBuilding.put("marketValue", "100000");

        Response response = given()
                .contentType(applicationJson)
                .accept(applicationJson)
                .body(updateBuilding)
                .pathParam("ownerId", ownerId)
                .pathParam("buildingId", buildingId)
                .when()
                .put("/owner/" + "{ownerId}" + "/building/" + "{buildingId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);
        assertEquals("Apartment", response.jsonPath().getString("propertyType"));
        assertEquals("Klaipeda", response.jsonPath().getString("city"));
        assertEquals("Kranto", response.jsonPath().getString("street"));
        assertEquals("35-2", response.jsonPath().getString("number"));
        assertEquals(75.0, Double.parseDouble(response.jsonPath().getString("size").toString()));
        assertEquals(100000.0, Double.parseDouble(response.jsonPath().getString("marketValue").toString()));
    }

    /**
     * test delete building
     */
    @Test
    void i() {
        Response response = given()
                .contentType(applicationJson)
                .accept(applicationJson)
                .pathParam("ownerId", ownerId)
                .pathParam("buildingId", buildingId)
                .when()
                .delete("/owner/" + "{ownerId}" + "/building/" + "{buildingId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);
        assertEquals(response.jsonPath().getString("operationResult"), "SUCCESS");
        assertEquals(response.jsonPath().getString("operationName"), "DELETE");
    }

    /**
     * Test delete owner details
     */
    @Test
    void j() {
        Response response = given()
                .contentType(applicationJson)
                .pathParam("ownerId", ownerId)
                .accept(applicationJson)
                .when()
                .delete("/owner/" + "{ownerId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);
        assertEquals(response.jsonPath().getString("operationResult"), "SUCCESS");
        assertEquals(response.jsonPath().getString("operationName"), "DELETE");
    }

    /**
     * Test create demo data
     */
    @Test
    void k() {
        Response response = given()
                .contentType(applicationJson)
                .accept(applicationJson)
                .when()
                .get("/owner/demoData")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);
        List<Object> owners = response.jsonPath().getList("");
        assertEquals(3, owners.size());

        Map<String, Object> vladas = (Map<String, Object>) owners.get(0);
        Map<String, Object> john = (Map<String, Object>) owners.get(1);
        Map<String, Object> hubert = (Map<String, Object>) owners.get(2);

        // Delete all 3 owners
        Response responseVladas = given()
                .contentType(applicationJson)
                .pathParam("ownerId", vladas.get("id"))
                .accept(applicationJson)
                .when()
                .delete("/owner/" + "{ownerId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(responseVladas);
        assertEquals(responseVladas.jsonPath().getString("operationResult"), "SUCCESS");
        assertEquals(responseVladas.jsonPath().getString("operationName"), "DELETE");

        Response responseJohn = given()
                .contentType(applicationJson)
                .pathParam("ownerId", john.get("id"))
                .accept(applicationJson)
                .when()
                .delete("/owner/" + "{ownerId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(responseJohn);
        assertEquals(responseJohn.jsonPath().getString("operationResult"), "SUCCESS");
        assertEquals(responseJohn.jsonPath().getString("operationName"), "DELETE");

        Response responseHubert = given()
                .contentType(applicationJson)
                .pathParam("ownerId", hubert.get("id"))
                .accept(applicationJson)
                .when()
                .delete("/owner/" + "{ownerId}")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(responseHubert);
        assertEquals(responseHubert.jsonPath().getString("operationResult"), "SUCCESS");
        assertEquals(responseHubert.jsonPath().getString("operationName"), "DELETE");

    }

    private void createOwnerDetails() {
        ownerDetails = new HashMap<>();
        ownerDetails.put("firstName", "Vladas");
        ownerDetails.put("lastName", "Uponavicius");
        ownerDetails.put("phone", "37060000000");
    }

    private void createBuild() {
        building = new HashMap<>();
        building.put("propertyType", "House");
        building.put("city", "Vilnius");
        building.put("street", "Vingio");
        building.put("number", "35");
        building.put("size", "180");
        building.put("marketValue", "180000");
    }
}
